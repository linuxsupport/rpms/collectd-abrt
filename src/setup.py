import os

from setuptools import setup, find_packages

version = '1.0.0'

install_requires = [
    'abrt',
    'abrt-cli'
]


setup(name='collectd_abrt',
    version=version,
    description="Collectd Plugin to count processes",
    classifiers=[
        "Development Status :: 4 - Beta",
        "Intended Audience :: System Administrators",
        "Operating System :: POSIX :: Linux",
        "Programming Language :: Python",
        "Topic :: System :: Monitoring",
    ],
    keywords='collectd abrt monitoring',
    url = 'https://gitlab.cern.ch/linuxsupport/rpms/collectd-python-abrt',
    author='Daniel Juarez Gonzalez',
    author_email='djuarezg@cern.ch',
    maintainer='CERN IT LCS',
    maintainer_email='linux-experts@cern.ch',
    license='Apache II',
    packages=find_packages('src'),
    package_dir = {'': 'src'},
    include_package_data=True,
    zip_safe=False,
    install_requires=install_requires,
)

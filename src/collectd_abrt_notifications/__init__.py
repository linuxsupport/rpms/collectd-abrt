import collectd
import subprocess
import time
import re

# Note:  Nodes need to have a FE assigned, be roger alarmed and have the abrt puppet module installed

# Default config
PLUGIN_NAME = 'abrt'
# Regex to match abrt-cli output
CRASHLINE_REGEX = re.compile(r'(\S+):\s*(.*)')
CRASHID_REGEX = re.compile(r'(\S+)\s*(\S+)')

# Make the check interval configurable
# Avoid checking all the time, otherwise we can get too much alarm flooding
CONFIG = {
    "interval": 300
}

def configure_callback(conf):
    global CONFIG
    for node in conf.children:
        key = node.key.lower()
        if key == 'interval':
            CONFIG['interval'] = int(node.values[0])
    # Register the read callback, it will check after each interval
    collectd.register_read(read_callback, CONFIG['interval'])

def read_callback():
    """ Read the abrt crashes, search for failures and send the info as a notification so it creates the GNI alarm. """
    # Get the current timestamp, only check crashes from the last interval check. Cast to int it as abrt-cli does not accept decimals
    timestamp = int(time.time())
    last_check = timestamp - CONFIG['interval']
    # This will come from the abrt puppet module
    # We cannot filter on the command line the recently crashed packages, as the --since option only works with the first occurrence timestamp
    # This means we need to filter it ourselves... high tech coming from here
    out_list = subprocess.Popen(['/usr/bin/abrt-cli', 'list'],
           stdout=subprocess.PIPE,
           stderr=subprocess.STDOUT)
    stdout,stderr = out_list.communicate()

    # Split per crash
    crashes = stdout.split("\n\n")
    # Each crash would look like:
    """
    [root@testabrtmoduledjg ~]#  abrt-cli list
    id c5002b469bc364861f90ae65e8b15df9e56aafd8
    reason:         sleep killed by SIGSEGV
    time:           Mon 29 Jun 2020 02:14:31 PM CEST
    cmdline:        sleep 10m
    package:        coreutils-8.22-24.el7
    uid:            106742 (djuarezg)
    count:          1
    Directory:      /var/spool/abrt/ccpp-2020-06-29-14:14:31-12388
    Reported:       https://abrtanalytics.web.cern.ch/faf/reports/bthash/5bb483b5ca0ea992b911f4d845479a085eab0f24
    """
    # Filter them by last occurrence, using id to have the details
    for crash in crashes:
        crash_lines = crash.split("\n")

        # First line allows us to check by id the last occurrence
        crash_id = CRASHID_REGEX.match(crash_lines[0]).group(2)
        # Very sofisticated finding of the last occurrence
        cmd = "/usr/bin/abrt-cli info {} -d | grep last_occurrence".format(crash_id)
        # shell=True so we can pipe
        last_ocurrence_output = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        stdout,stderr = last_ocurrence_output.communicate()
        last_ocurrence_match = CRASHLINE_REGEX.match(stdout).group(2)

        # We process only crashes that happened from the last time we checked
        if int(last_ocurrence_match) >= int(last_check):
            # Now we no longer need the id, get rid of it for the sake of parsing lines as key: value
            del crash_lines[0]
            crash_dict = {}
            for line in crash_lines:
                crash_info_match = CRASHLINE_REGEX.match(line)
                if crash_info_match is None:
                    continue
                else:
                    crash_dict[crash_info_match.group(1)] = crash_info_match.group(2)
            # We now have to parse the received information, for each crash, trigger a notification
            # Each notification has a max length message of 255 chars, so we split each report and dispatch them individually
            try:
                # Max 255 chars, get straight to the point!!!
                crash_report = '{} crashed due to "{}"; cmdline was "{}"; triggered by uid "{}".'.format(crash_dict['package'], crash_dict['reason'], crash_dict['cmdline'], crash_dict['uid'])
                # Add FAF url if existent
                if crash_dict['Reported']:
                    crash_report = crash_report + " Reported to {}".format(crash_dict['Reported'])
                # Dispatch, type cannot accept any string, but I could not find which ones are accepted
                x = collectd.Notification(plugin=PLUGIN_NAME, plugin_instance="crash", type="count", message=crash_report)
                # Set as a FAILURE to send the GNI alarm
                x.severity = collectd.NOTIF_FAILURE
                x.dispatch()
            except Exception as e:
                print(e.__doc__)
                print(e.message)


collectd.register_config(configure_callback)

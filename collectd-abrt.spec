%global py_name collectd-abrt

Name:      collectd-abrt
Version:   1.0.3
Release:   1%{?dist}
Summary:   Collectd Plugin to notify about rpm crashes
Vendor:    CERN
License:   GPL
Group:     System Environment/Base
URL:       https://gitlab.cern.ch/linuxsupport/rpms/collectd-abrt
Source:    %{name}-%{version}.tar.gz

BuildArch:      noarch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root

AutoReq:        no
Requires:       collectd

%if 0%{?el7}
%global __python /usr/bin/python2
%global python_sitelib %{python2_sitelib}
BuildRequires:  python2-devel
BuildRequires:  python2-setuptools
%else
%if 0%{?el8}
%global __python /usr/bin/python3
%global python_sitelib %{python3_sitelib}
BuildRequires:  python3-devel
BuildRequires:  python3-setuptools
%endif
%endif


%description
Collectd Plugin template

%prep
%setup -q

%build
CFLAGS="%{optflags}" %{__python} src/setup.py build

%install
%{__rm} -rf %{buildroot}
%{__python} src/setup.py install --skip-build --root %{buildroot}

%clean
%{__rm} -rf %{buildroot}

%post -n %{py_name}
/sbin/service collectd condrestart >/dev/null 2>&1 || :

%postun -n %{py_name}
if [ $1 -eq 0 ]; then
    /sbin/service collectd condrestart >/dev/null 2>&1 || :
fi

%files
%defattr(-,root,root)
%{python_sitelib}/*

%changelog
* Thu Jul 16 2020 - Daniel Juarez <djuarezg@cern.ch> - 1.0.3-1
- Allow users to specify the checking interval

* Wed Jul 15 2020 - Daniel Juarez <djuarezg@cern.ch> - 1.0.2-1
- Rename python module to ease logic understanding

* Wed Jul 15 2020 - Daniel Juarez <djuarezg@cern.ch> - 1.0.1-1
- Minor fixes
- Python2 for cc7 and Python3 for c8

* Wed Jul 08 2020 - Daniel Juarez <djuarezg@cern.ch> - 1.0.0-1
- Initial release

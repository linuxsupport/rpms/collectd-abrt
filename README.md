# collectd-abrt

This plugin will trigger GNI alarms when abrt detects an [`abrt`](https://github.com/abrt/abrt) crash.

Due to the limitation in size of `collectd` notification messages, you should manually check the node for further investigating and debugging.

Remember to check <http://monitdocs.web.cern.ch/monitdocs/alarms/collectd.html#snow> on how to control GNI alarms features.


# Logic

Every check interval the plugin will check for any abrt crash that happened during this interval, and create **one `collectd` notification per abrt crash**. 
This notification will create the corresponding GNI alarm according to your node configuration if they have a FE assigned, are roger alarmed and have [the abrt puppet module](https://gitlab.cern.ch/ai/it-puppet-module-abrt) installed.

A sample GNI alarm would be:

```
Alarm: abrt_crash_count
Metrics: abrt_count
Status: FAILURE
Entities: testabrtmoduledjg
Producer: collectd
Grouping: Hostgroup / Cluster
Hostgroup: playground/djuarezg
Collectd namespace: abrt_crash_count
will-crash-0.9-1.el7 crashed due to "will_abort killed by SIGABRT"; cmdline was "will_abort"; triggered by uid "0 (root)". Reported to https://abrtanalytics.web.cern.ch/faf/reports/bthash/75436c31eb1ee7dfbf75e9cf7f251d4824473350
```

# Usage

Add it to your hostgroup as it follows:

```
class { 'cerncollectd_contrib::plugin::abrt_notifications': }
```

Or if you want to specify the check interval in seconds (by default it will only check for crashes that happened in the last 5min, every 5min):

```
class { 'cerncollectd_contrib::plugin::abrt_notifications':
  interval => 30
}
```

**Note:  Nodes need to have a FE assigned, be roger alarmed and have the abrt puppet module installed in order for GNI alarms to be created**

## Whitelisting

As referenced on the [Monitoring Documentation site](http://monitdocs.web.cern.ch/monitdocs/ingestion/collectd.html#servicenative), you should ask for your hostgroup to
 be whitelisted if you decide to use this plugin, unless your hostrgroup was already added in the past.
